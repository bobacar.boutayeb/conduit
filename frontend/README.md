# Conduit frontend

This is the frontend of the Conduit project.


## Vue3 codebase

We've gone to great lengths to adhere to the **Vue3** community styleguides & best practices.


## What works?

- [x] [Vite](https://github.com/vitejs/vite)
- [x] [Composition API](https://composition-api.vuejs.org/)
- [x] [SFC \<script setup> sugar](https://v3.vuejs.org/api/sfc-script-setup.html)
- [x] [Suspense](https://v3.vuejs.org/guide/component-dynamic-async.html#using-with-suspense) (Experimental)
- [x] [Vue router](https://next.router.vuejs.org/)
- [x] [Pinia](https://pinia.vuejs.org/) for state management
- [x] [TypeScript](https://www.typescriptlang.org/) and [Vue tsc](https://github.com/johnsoncodehk/vue-tsc) for static analysis
- [x] [ESLint](https://eslint.vuejs.org/) for syntax checking and code styling
- [x] Component test ([Cypress component testing](https://docs.cypress.io/guides/component-testing))
- [x] E2E test ([Cypress](https://docs.cypress.io))

## Getting started

```shell script
yarn install

# Development
yarn dev

# Build dist
yarn build

# Run unit tests
yarn test:unit
yarn test:unit:ci

# Run E2E tests
yarn test:e2e
yarn test:e2e:ci
```
